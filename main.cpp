/* mbed Microcontroller Library
 * Copyright (c) 2019 ARM Limited
 * SPDX-License-Identifier: Apache-2.0
 */

#include "AnalogIn.h"
#include "DigitalOut.h"
#include "PinNames.h"
#include "mbed.h"
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include "BufferedSerial.h"
#include "mbed_thread.h"
#include "stdlib.h"


#define MAX_PC_BUFFER_SIZE 10
#define MAX_RS232_BUFFER_SIZE 10
#define THRESHOLD_10_PERCENT 6554
#define THRESHOLD_90_PERCENT 58982
#define TX_FOR_RS232 PC_10
#define RX_FOR_RS232 PC_11


// Create a serial object "pc" for communication between PC and board
static BufferedSerial pc(USBTX, USBRX);

static BufferedSerial rs232(TX_FOR_RS232, RX_FOR_RS232);

// Set analog inputs and digital outputs for Resistance and Temperature measurement
AnalogIn res_input(A4);
AnalogIn temp_input(A5);

DigitalOut Res_A(D7);
DigitalOut Res_B(D9);
DigitalOut Sel_Res_A(D3);
DigitalOut Sel_Res_B(D4);
DigitalOut Sel_Res_C(D5);

static int reference_resistor;

DigitalOut PT100_A(D11);
DigitalOut PT100_B(D12);

void set_reference_resistor(int reference){
  if (reference==0){
    Sel_Res_A = 0;
    Sel_Res_B = 1;
    Sel_Res_C = 0;
  } else if (reference==1) {
    Sel_Res_A = 1;
    Sel_Res_B = 1;
    Sel_Res_C = 0;
  } else if (reference==2) {
    Sel_Res_A = 0;
    Sel_Res_B = 0;
    Sel_Res_C = 1;
  } else if (reference==3) {
    Sel_Res_A = 1;
    Sel_Res_B = 0;
    Sel_Res_C = 1;
  }else if (reference==4) {
    Sel_Res_A = 0;
    Sel_Res_B = 1;
    Sel_Res_C = 1;
  }else if (reference==5) {
    Sel_Res_A = 1;
    Sel_Res_B = 1;
    Sel_Res_C = 1;
  }else {
    Sel_Res_A = 0;
    Sel_Res_B = 1;
    Sel_Res_C = 0;
  }
}

int main(void)
{
  char pc_uart_buff[MAX_PC_BUFFER_SIZE] = {};
  char rs232_buff[MAX_RS232_BUFFER_SIZE] = {};

  pc.set_baud(9600);
  pc.set_format(8, BufferedSerial::None, 1);

  rs232.set_baud(9600);
  rs232.set_format(8, BufferedSerial::None, 1);

  int res_channel = 1;
  int temp_channel = 1;
  Res_A = 0;
  Res_B = 0;
  // 
  Sel_Res_A = 0;
  Sel_Res_B = 0;
  Sel_Res_C = 1;

  reference_resistor = 2;

  PT100_A = 0;
  PT100_B = 0;

  while (1) {
    if (pc.readable()) {
      thread_sleep_for(200);
      pc.read(pc_uart_buff, 1);
      //printf("I got '%s'\n", pc_uart_buff);

      switch (pc_uart_buff[0]) {
        case 't':
          //printf("Proceeding Temperature measurement.\n");
          pc.read(pc_uart_buff, 1);
          temp_channel = atol(pc_uart_buff);
          //printf("Channel number: %d\n", temp_channel);
          if (temp_channel == 1){
            //printf("Channel 1\n");
            PT100_A.write(0);
            PT100_B.write(0);
          }else if (temp_channel==2) {
            //printf("Channel 2\n");
            PT100_A.write(1);
            PT100_B.write(0);
          }else if (temp_channel==3) {
            //printf("Channel 3\n");
            PT100_A.write(0);
            PT100_B.write(1);
          }else if (temp_channel==4) {
            //printf("Channel 4\n");
            PT100_A = 1;
            PT100_B = 1;
          }else{
            //printf("Wrong channel number\n");
            break;
          }
          //printf("Getting measures...\n");
          for (int i = 0; i <2; i++){
            uint16_t sample = temp_input.read_u16();
            printf("%hu,%d,%d\n", sample, PT100_A.read(), PT100_B.read());
            thread_sleep_for(500);
          }
          break;

        case 'r':
          //printf("Proceeding Resistance measurement. Waiting for channel number.\n");
          pc.read(pc_uart_buff, 1);
          res_channel = atol(pc_uart_buff);
          //printf("Channel number: %d\n", res_channel);
          if (res_channel == 1){
            //printf("Channel 1\n");
            Res_A.write(0);
            Res_B.write(0);
          }else if (res_channel==2) {
            //printf("Channel 2\n");
            Res_A.write(1);
            Res_B.write(0);
          }else if (res_channel==3) {
            //printf("Channel 3\n");
            Res_A.write(0);
            Res_B.write(1);
          }else if (res_channel==4) {
            //printf("Channel 4\n");
            Res_A = 1;
            Res_B = 1 ;
          }else{
            //printf("Wrong channel number\n");
            break;
          }
          for (int i = 0; i < 2; i++){
            
            if (reference_resistor<0 || reference_resistor>5){
              reference_resistor = 2;
              Sel_Res_A = 0;
              Sel_Res_B = 0;
              Sel_Res_C = 1;
            }
            uint16_t sample = res_input.read_u16();
            printf("%hu,%d,%d,%d,%d\n", sample, reference_resistor, Sel_Res_A.read(), Sel_Res_B.read(), Sel_Res_C.read());


            if (sample < THRESHOLD_10_PERCENT){
              reference_resistor++;
              set_reference_resistor(reference_resistor);
              //printf("Increment reference resistor index\n");
            }else if (sample > THRESHOLD_90_PERCENT) {
              //printf("Decrement reference resistor index\n");
              reference_resistor--;
              set_reference_resistor(reference_resistor);
            }
            
            
            thread_sleep_for(500);
          }
          break;

        case 's':
          //INITIALIZE RS232 COMMUNICATION
          pc.read(pc_uart_buff, 10);
          thread_sleep_for(100);
          rs232.write(pc_uart_buff, 10);
          rs232.read(rs232_buff, 10);
          thread_sleep_for(100);
          pc.write(rs232_buff, 10);
          break;

        default:
          //printf("No function added\n");
          break;

      }
    }
  }
}
